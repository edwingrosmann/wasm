### Introduction
This application shows how to display Druid-animations in a web-page.
Think WASM. 
There are two animations available:
### A 100fps Thingamadoodle
![Animation](pics/Druid%20based%20WASM%20Animation.png)

### A Resizable Analogue Clock
![Clock](pics/Druid%20based%20WASM%20Clock.png) 

### How to run this WASM Application

From terminal in applcation root:

1. ```cargo install https``` (a one-off)
1. ```wasm-pack build --target web --dev```
1. ```http```
1. Then open browser at http://localhost:8000

### Special Mention

* A Rust WASM application cannot directly create a time: e.g. ```chrono::Utc::now()```. One has to request current time from the Javascript host; see [time.rs](time.rs) how that's done! Spoiler altert: crate ```js_sys``` to the rescue.


