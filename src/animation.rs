use std::f64::consts::PI;
use std::time::Duration;

use druid::{LifeCycle, LifeCycleCtx, LocalizedString, theme};
use druid::{AppLauncher, Data, Env, Event, WindowDesc};
use druid::{
    BoxConstraints, Color, EventCtx, LayoutCtx, PaintCtx, Point, RenderContext, Size, UpdateCtx,
    Vec2,
};
use druid::kurbo::Line;
use druid::piet::{FontBuilder, Text, TextLayoutBuilder};
use druid::widget::Widget;

use crate::settings::{test_mode};

#[derive(Debug, Clone, Default)]
struct UiModel {
    t: f64,
    step: f64,
    running: bool,
}

impl Data for UiModel {
    fn same(&self, _: &Self) -> bool {
        true
    }
}

pub fn main() {
    AppLauncher::with_window(
        WindowDesc::new(move || UiModel::default()).title(LocalizedString::new("Cool Animation")
                                                              .with_placeholder("Click me please..."),
        ).window_size((800.0, 600.0)), )
        .launch(UiModel::default())
        .expect("launch failed");
}

const MAX_RUN_TIME: f64 = 2.00000;
const INITIAL_STEP_SIZE: f64 = 0.005;

impl Widget<UiModel> for UiModel {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, _data: &mut UiModel, _env: &Env) {
        match event {
            Event::MouseDown(_) => {
                self.step = INITIAL_STEP_SIZE;
                self.running = !self.running;
                ctx.request_anim_frame();
            }
            Event::Timer(_t) => {
                ctx.request_anim_frame();
            }
            _ => {}
        }
    }

    fn lifecycle(&mut self, ctx: &mut LifeCycleCtx, event: &LifeCycle, _data: &UiModel, _env: &Env) {
        match event {
            LifeCycle::WidgetAdded => {
                println!("Starting UI", );

                //Kick off the animation when testing...
                self.request_anim_frame_when_testing(ctx);
            }
            LifeCycle::AnimFrame(_) => {
                if !self.running {
                    return;
                }

                self.t += self.step;//(*interval as f64) * 1e-10;

                if self.step.abs() > 0.15 {
                    self.step = INITIAL_STEP_SIZE;
                } else if (self.step < 0.0 && self.t < 0.0) || (self.step > 0.0 && self.t > MAX_RUN_TIME) {
                    self.step *= -1.25;
                }
                ctx.request_timer(Duration::from_millis(10));

                self.close_window_at_end_when_testing(ctx)
            }
            _ => ()
        }
    }

    fn update(
        &mut self,
        _ctx: &mut UpdateCtx,
        _old_data: &UiModel,
        _data: &UiModel,
        _env: &Env,
    ) {
        println!("Updating: {}", self.t);
    }

    fn layout(
        &mut self,
        _layout_ctx: &mut LayoutCtx,
        bc: &BoxConstraints,
        _data: &UiModel,
        _env: &Env,
    ) -> Size {
        bc.constrain((650.0, 650.0))
    }

    fn paint(&mut self, paint_ctx: &mut PaintCtx, _data: &UiModel, env: &Env) {
        let center = Point::new(150.0, 150.0);
        let ambit = center + 145.0 * Vec2::from_angle((0.75 + self.t) * 2.0 * PI);
        paint_ctx.stroke(Line::new(center, ambit), &Color::rgb8(255, 0, 125), 20.0);
        let center2 = Point::new(350.0, 150.0);
        let ambit2 = center2 + 145.0 * Vec2::from_angle((-0.75 - self.t) * 2.0 * PI);
        paint_ctx.stroke(Line::new(center2, ambit2), &Color::rgb8(125, 0, 255), 10.0);
        paint_ctx.stroke(Line::new(ambit, ambit2), &Color::rgb8(255, 0, 255), 10.0);
        let center2 = Point::new(150.0, 150.0);
        let ambit2 = center2 + 145.0 * Vec2::from_angle((-0.75 - self.t) * 2.0 * PI);
        paint_ctx.stroke(Line::new(center2, ambit2), &Color::rgb8(0, 125, 255), 10.0);
        let center = Point::new(350.0, 150.0);
        let ambit = center + 145.0 * Vec2::from_angle((0.75 + self.t) * 2.0 * PI);
        paint_ctx.stroke(Line::new(center, ambit), &Color::rgb8(0, 255, 125), 20.0);
        paint_ctx.stroke(Line::new(ambit, ambit2), &Color::rgb8(0, 255, 255), 10.0);

        let font_name = env.get(theme::FONT_NAME);
        let font_size = env.get(theme::TEXT_SIZE_NORMAL) * 2.0;
        let mut t = paint_ctx.text();
        let f = t
            .new_font_by_name(font_name, font_size)
            .build()
            .expect("Notte di Font no found");
        let tl = t
            .new_text_layout(&f, &*format!("Step-size: {}", self.step), 1000_f64)
            .build()
            .expect("Ai can notti di maik");

        let tl2 = t
            .new_text_layout(&f, format!("{}", ambit).as_str(), 1000_f64)
            .build()
            .expect("Aiij can notti di maik");

        paint_ctx.draw_text(&tl, Point::new(150.0, 350.0), &Color::rgb8(255, 125, 125));
        paint_ctx.draw_text(&tl2, Point::new(150.0, 450.0), &Color::rgb8(0, 225, 125));
    }
}

impl UiModel {
    fn request_anim_frame_when_testing(&mut self, ctx: &mut LifeCycleCtx) {
        if test_mode() {
            //Skip to the end...
            self.t = MAX_RUN_TIME - 0.2;
            ctx.request_anim_frame();
        }
    }

    fn close_window_at_end_when_testing(&mut self, ctx: &mut LifeCycleCtx) {
        if self.t > MAX_RUN_TIME && test_mode() {
            ctx.window().close();
        }
    }
}

