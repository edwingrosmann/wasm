#![warn(warnings)]
#![macro_use]
#![feature(const_fn)]
pub mod animation;
pub mod druidclock;
pub mod settings;
mod time;

#[cfg(test)]
mod tests;

/// In order for this binary crate to run integration tests, both lib.rs AND main.rs have to be present.
/// main::main() call this function.
/// See more on this topic: https://doc.rust-lang.org/book/ch11-03-test-organization.html#integration-tests-for-binary-crates
pub fn main(test_mode: bool, args: Vec<String>) {
    settings::set_args(args);
    settings::set_test_mode(test_mode);

    druidclock::main();
//    animation::main();
}

// Copyright 2020 The Druid Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use wasm_bindgen::prelude::*;


// This macro constructs a `wasm_bindgen` entry point to the given example from the examples
// directory.
macro_rules! impl_example {
    ($wasm_fn:ident, $expr:expr) => {
        #[wasm_bindgen]
        pub fn $wasm_fn() {
            std::panic::set_hook(Box::new(console_error_panic_hook::hook));
            $expr;
        }
    };
    ($fn:ident) => {
        impl_example!($fn, $fn::main());
    };
}

impl_example!(animation);
impl_example!(druidclock);
