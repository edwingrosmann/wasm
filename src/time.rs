use chrono::{DateTime,FixedOffset,NaiveDateTime};

pub fn get_date() -> DateTime<FixedOffset> {
    let t: TimeWrapper = js_sys::Date::new_0().into();
    t.0
}

struct TimeWrapper(DateTime<FixedOffset>);

impl From<js_sys::Date> for TimeWrapper {
    fn from(date: js_sys::Date) -> TimeWrapper {
        let offset_secs = (date.get_timezone_offset() * 60.0) as i32;
        let millisecs_since_unix_epoch: u64 = date.get_time() as u64;
        let secs = millisecs_since_unix_epoch / 1000;
        let nanos = 1_000_000 * (millisecs_since_unix_epoch % 1000);
        let naive = NaiveDateTime::from_timestamp(secs as i64, nanos as u32);
        TimeWrapper(DateTime::from_utc(naive, FixedOffset::west(offset_secs)))
    }
}
