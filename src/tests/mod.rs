mod settings_test;

pub fn args_for(args: Vec<&str>) -> Vec<String> {
    args.iter().map(|s| s.to_string()).collect()
}
